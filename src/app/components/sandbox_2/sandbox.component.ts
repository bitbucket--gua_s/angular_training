import { Component } from '@angular/core';

@Component({
    selector : 'app-sandbox',
    templateUrl : './sandbox.component.html',
    styleUrls: ['./sandbox.component.css']
})

export class SandboxComponent {
    text = 'Random Text';
    value = true;
    text_2 = 'Random Text';

    // Change Value with Events
    changeValue() {
        this.text = 'Changed';
    }
    booleanChange() {
        // Triggers toggleability
        this.value = !this.value;
    }
    // Key Events
    keyEvent(e) {
        console.log(e.type);
    }
    changeText(e) {
        this.text_2 = e.target.value;
    }
    // Trigger Events
    fireEvent(e, greeting) {
        console.log(greeting);
    }
}
