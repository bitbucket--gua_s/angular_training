import { Component } from '@angular/core';
import { DataService } from '../../services/data.service';

@Component({
    selector : 'app-sandbox',
    templateUrl : './sandbox.component.html',
    styleUrls: ['./sandbox.component.css']
})

export class SandboxComponent {
    // to initiate DataService
    users: string[];
    data: any[] = [];
    // Http request
    usersHttp: any[];
    // From form
    userHttp = {
        id: '',
        name: '',
        email: '',
        phone: ''
    };
    isEdit: boolean;
    constructor(public dataService: DataService) {
        this.users = this.dataService.getUsers();
        // console.log(this.dataService.getUsers());
        this.dataService.getData().subscribe(data => {
            this.data.push(data);
            // console.log(data);
        });
        this.dataService.getJson().subscribe(usersHttp => {
            this.usersHttp = usersHttp;
            // console.log(usersHttp);
        });
    }

    onSubmit(isEdit) {
        if (isEdit) {
            this.dataService.updateUser(this.userHttp).subscribe(userHttp => {
                // For loop to look for the user id
                for (let i = 0; i < this.usersHttp.length; i++) {
                    // If match splice match out from array
                    if (this.usersHttp[i].id === this.userHttp.id) {
                        this.usersHttp.splice(i, 1);
                    }
                }
                // Add Edit back to the Array
                this.usersHttp.unshift(this.userHttp);
            });
        } else {
            this.dataService.addUser(this.userHttp).subscribe(userHttp => {
                console.log(userHttp);
                this.usersHttp.unshift(userHttp);
            });
        }
    }
    onDeleteClick(id) {
        this.dataService.deleteUser(id).subscribe(res => {
            for (let i = 0; i < this.usersHttp.length; i++) {
                if (this.usersHttp[i].id === id) {
                    this.usersHttp.splice(i, 1);
                }
            }
            console.log(res);
        });
    }
    onEditClick(userHttp) {
        this.isEdit = true;
        this.userHttp = userHttp;
    }
}
