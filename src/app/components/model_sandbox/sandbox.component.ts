import { Component } from '@angular/core';

@Component({
    selector : 'app-sandbox',
    templateUrl : './sandbox.component.html',
    styleUrls: ['./sandbox.component.css']
})

export class SandboxComponent {
    // Proper syntax for type properties
    // https://angular.io/guide/styleguide#style-05-14
    userName: string;
    userEmail: string;
    userPhone: number;
    user = {
        userName: this.userName,
        userEmail: this.userEmail,
        userPhone: this.userPhone
    };
    // Form validation
    // { object } !!
    onSubmit({value, valid}) {
        if (valid) {
            console.log('success!', value);
        } else {
            console.log(value);
        }
    }
}
