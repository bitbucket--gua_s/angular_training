import { Component } from '@angular/core';

@Component({
    selector : 'app-sandbox',
    templateUrl : './sandbox.component.html',
    // Incase needed styles can be apply inside of the component decorator.
    // styles: [`
    // .special{
    //     color:green;
    //     font-size: 40px;
    // }`]
    // Default component decorator
    styleUrls: ['./sandbox.component.css']
})

export class SandboxComponent {
    // String types
    // tslint:disable-next-line:no-inferrable-types
    name: string = 'John Doe';
    // tslint:disable-next-line:no-inferrable-types
    age: number = 30;
    // tslint:disable-next-line:no-inferrable-types
    hasChildren: boolean = true ;
    person: any = {
        firstName : 'Steve',
        lastName : 'Smith',
        age : '28'
    };
    // When its needed the types inside of an array can be fixed to its index
    tupleArray: [string, number] = ['John Doe', 29];
    unusable: void = undefined;
    undef: undefined = undefined;
    nool: null = null;

    people: string[] = ['Rick', 'Daryl', 'Carl', 'Glen'];
    people_2: object[] = [
        {
            firstName: 'Rick',
            lastName: 'Sanchez',
            showName: true,
            greeting: 1,
            hasChange: false
        },
        {
            firstName: 'Daryl',
            lastName: 'Dixon',
            showName: true,
            greeting: 2,
            hasChange: true
        },
        {
            firstName: 'Carl',
            lastName: 'Perez',
            showName: false,
            greeting: 3,
            hasChange: false
        },
        {
            firstName: 'Glen',
            lastName: 'Rhee',
            showName: true,
            greeting: 4,
            hasChange: true
        },
    ];
    imageUrl = 'http://lorempixel.com/200/200';
    isSpecial = true;
    canSave = true;
    currentStyles = {};
    setCurrentStyles() {
        this.currentStyles = {
            'font-style': this.canSave ? 'italic' : 'normal',
            'font-size': this.isSpecial ? '24px' : '12px'
        };
    }
    // tslint:disable-next-line:member-ordering
    fecha = new Date();
    // tslint:disable-next-line:member-ordering
    dinero = 500;
    // tslint:disable-next-line:member-ordering
    fee = 0.04;
    constructor() {
        this.setCurrentStyles();
        console.log('Constructor ran...');
    }
}
