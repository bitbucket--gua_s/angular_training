// Service: is a Class that can send functionality and data across multiple components
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { Http } from '@angular/http';
// Adds reactivity
import 'rxjs/add/operator/map';

@Injectable() export class DataService {
    users: string[];
    data: Observable<Array<number>>;


    constructor(public http: Http) {
        this.users = ['Mark', 'Sharon', 'Beth', 'John'];
    }

    getUsers() {
        return this.users;
    }

    getData() {
        this.data = new Observable(observer => {
            setTimeout(() => {
                observer.next([1]);
            }, 1000);
            setTimeout(() => {
                observer.next([2]);
            }, 2000);
            setTimeout(() => {
                observer.next([3]);
            }, 3000);
            setTimeout(() => {
                observer.next([4]);
            }, 4000);
            setTimeout(() => {
                observer.complete();
            }, 5000);
        });
        return this.data;
    }
    getJson() {
        // Points to fake API
        // http://jsonplaceholder.typicode.com
        return this.http.get('http://jsonplaceholder.typicode.com/users').map(res => res.json());
    }
    addUser(userHttp) {
        return this.http.post('http://jsonplaceholder.typicode.com/users', userHttp).map(res => res.json());
    }
    deleteUser(id) {
        return this.http.delete('http://jsonplaceholder.typicode.com/users/' +  id).map(res => res.json());
    }
    updateUser(userHttp) {
        return this.http.put('http://jsonplaceholder.typicode.com/users/' +  userHttp.id, userHttp).map(res => res.json());
    }
}
